microservices
-------------

Тестовое задание соискателя вакансии программитста Go (Go-lang).

## Условия необходимые для успешного запуска и работы микросервисов

1. Микросервисы взаимодействуют между собой через очередь сообщений использующую сетевое журналируемое хранилище redis.
redis должен быть сконфигурирован таким образом чтобы с ним можно было работать не указывая пароля клиента (настройки поумолчанию). 

2. В качестве Go-клиента redis используется пакет go-redis.v3 https://github.com/go-redis/redis.
Чтобы установить пакет go-redis необходимо выполнить команду ```go get gopkg.in/redis.v3```.


## Порядок сборки, запуска и проверки работы микросервисов

1. Собрать исполняемые файлы выполнив скрипт ```build_all.sh``` (не забываем выставить права доступа на запуск).
2. Запустить терминал, выполнить команду ```vorker```.
3. Запустить второй терминал, выполнить команду ```server```.
4. Запустить третий терминал, выполнить команду ```curl localhost:8080/params/?1,2,5,160```, наблюдать ответ ```Summary value numeric params =  168```. В случае если утилита curl не установлена (а устанавливать лениво), можно запрос ```localhost:8080/params/?1,2,5,160``` ввести в адресную строку веб-браузера.