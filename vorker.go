/* 
    Воркер процесс читающий из очереди очередное сообщение и выполняющий
    обработку данных содержащиеся в нём.
*/
package main

import (
    "log"
    "strings"
    "strconv"
    "microservices/queue"
)


func main() {
   
  log.SetPrefix("L> ")    
  log.Println("Vorker begin ...") 
    
  q := queue.NewQueue("localhost:6379", "",  0) 
  if !q.IsAvailable() {
    log.Fatal("Queue is not available.")
  }
  
  for {    
    message := q.BDeque("vorker")
    
    if message == "QUIT" { // INFO: Предусмотрим возможность остановит сервис "в штатном режиме".
      break  
    }
        
    log.Println("Recive message = ", message)
    data := parser(message)
    q.Enque(data[0], algorithm(data[1:])) 
  }
    
  log.Println("End.")
}


/*  Расшифровка стркутуры данных для обмена между микросервисами.
*/ 
func parser(data string) []string {

  return strings.Split(data, ",")

}


/*  Выполнение некоторых действий над данными.
*/ 
func algorithm(data []string) string {

  sum := 0

  i := 0
  for i< len(data) {
    if value, err := strconv.Atoi(data[i]); err == nil {
        sum += value 
    } 
    
    i++
  }

  return strconv.Itoa(sum)
}


