/* 
  HTTP server принимает данные от клиента формирует сообщения и помещает
  их в очередь для дальнейшей обработки воркером. 
*/
package main

import (
    "fmt"
    "log"
    "net/http"
    "strconv"
    "microservices/queue"
    "microservices/idgenerator"
)


var remouteQueue *queue.Queue
var uniqueID *idgenerator.IDGenerator


func main() {
   
  log.SetPrefix("L> ")    
  log.Println("Server is runing ...") 
    
  remouteQueue = queue.NewQueue("localhost:6379", "",  0)
  if !remouteQueue.IsAvailable() {
    log.Fatal("Queue is not available.")
  }
  
  uniqueID = idgenerator.NewIDGenerator()
      
  http.HandleFunc("/", root_handler)
  http.HandleFunc("/params/", params_handler)
  if err := http.ListenAndServe(":8080", nil); err != nil {
      log.Fatal("failed to start server", err)
  }

  log.Println("End.")
}



/*  Обработчик HTTP запроса. URL "domainName/" используется для "тестирования связи" с сервером. 
*/ 
func root_handler(w http.ResponseWriter, r *http.Request) {
  fmt.Fprintf(w, "Welcome, %s", r.URL.Path[1:])
}
 
 
/*  Обработчик HTTP запроса по URL вида "domainName/params/<p1,p2,p3 ... >".  
*/ 
func params_handler(w http.ResponseWriter, r *http.Request) {
    
  var id = uniqueID.GetNextID()
  var message string
    
  message = strconv.FormatInt(id, 10) + "," + r.URL.RawQuery
  remouteQueue.Enque("vorker", message)
      
  resultFromVorker := remouteQueue.BDeque(strconv.FormatInt(id, 10))
  fmt.Fprintf(w, "Summary value numeric params =  %s", resultFromVorker)
}


