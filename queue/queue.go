/* Пакет queue реализует очередь для асинхронного обмена сообщения между микросервисами.

  API очереди предусматривает указания идентификатора микросервиса, для котрого предназгачено сообщение.  
  
*/
package queue

import "gopkg.in/redis.v3"


type Queue struct {
  сlient *redis.Client
}


/*  Конструктор очереди.
*/
func NewQueue(HostAddress, Passwd string , DataBase int64) *Queue {
  
  var q Queue
  
  q.сlient = redis.NewClient(&redis.Options{
        Addr:     HostAddress,
        Password: Passwd,
        DB:       DataBase,
    }) 
    
  return &q
} 


/*  Проверяет доступность очереди. 
*/
func (q *Queue) IsAvailable() bool {

  pong, err := q.сlient.Ping().Result()
  
  if (err == nil) && (pong == "PONG") {
    return true
  } else {
    return false
  }
} 


/*  Помещает сообщение в очередь.
*/
func (q *Queue) Enque(id, message string) bool {
   
   if err := q.сlient.LPush(id, message).Err(); err != nil {
      return false
   } else {
    return true
   }     
} 


/*  Достаёт сообщение из очереди.
*/
func (q *Queue) BDeque(id string) string {
   
  if strings, err := q.сlient.BRPop(0, id). Result (); err != nil {
    return ""
  } else {
    return strings[1]
  }     
} 


