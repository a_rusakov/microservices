
package queue

import "testing"


/* Отсутствие доступа к серверу redis на локальном хосте имитируем изменением TCP порта
   6379 поумолчанию, заменяем на 6377).
*/
func Test_IsAvailable(t *testing.T) {
    
  q := NewQueue("localhost:6379", "",  0) 
  
  ReturnValue := q.IsAvailable()
  if ReturnValue != true {
    t.Error("Expected true, got ", ReturnValue)
  }

  q = NewQueue("localhost:6377", "",  0) 
  
  ReturnValue = q.IsAvailable()
  if ReturnValue != false {
    t.Error("Expected false, got ", ReturnValue)
  }
}


/*  Заполняем очередь сообщениями чередуя значения ключей.
    Затем, выбираем сообщения последовательно для каждого значения ключа.
    Убеждаемся, что прочитанные из очереди сообщения для каждого значения ключа
    соответсвуют последовательности их записи в очередь.  
*/
func Test_queue(t *testing.T) {
    
  q := NewQueue("localhost:6379", "",  0) 
  
  q.Enque("key1", "message 1_1")
  q.Enque("key1", "message 1_2")
  q.Enque("key3", "message 3_1")
  q.Enque("key1", "message 1_3")
  q.Enque("key2", "message 2_1")
  q.Enque("key1", "message 1_4")
  q.Enque("key2", "message 2_2")
  q.Enque("key1", "message 1_5")


  message := q.BDeque("key1")
  if message != "message 1_1" {
    t.Error("Expected message 1_1, got ", message)
  }

  message = q.BDeque("key1")
  if message != "message 1_2" {
    t.Error("Expected message 1_2, got ", message)
  }

  message = q.BDeque("key1")
  if message != "message 1_3" {
    t.Error("Expected message 1_3, got ", message)
  }

  message = q.BDeque("key1")
  if message != "message 1_4" {
    t.Error("Expected message 1_4, got ", message)
  }

  message = q.BDeque("key1")
  if message != "message 1_5" {
    t.Error("Expected message 1_5, got ", message)
  }

  message = q.BDeque("key2")
  if message != "message 2_1" {
    t.Error("Expected message 2_1, got ", message)
  }

  message = q.BDeque("key2")
  if message != "message 2_2" {
    t.Error("Expected message 2_2, got ", message)
  }

  message = q.BDeque("key3")
  if message != "message 3_1" {
    t.Error("Expected message 3_1, got ", message)
  }
}


