/* Пакет idgenerator реализует потокобезобасный генератор уникальных числовых значений.

  В данной реализации уникальным значением является целое число, типа int64.  
  
*/
package idgenerator


import (
    "sync"
)


type IDGenerator struct {
  id int64
  mutex *sync.RWMutex
}


/*  Конструктор объекта генератора.
*/
func NewIDGenerator() *IDGenerator {

  return &IDGenerator{0, new(sync.RWMutex)}
} 


/*  Возврашает очередное значение уникального идентификатора.
*/
func (g *IDGenerator) GetNextID() int64 {

  g.mutex.Lock()
  defer g.mutex.Unlock()

  g.id ++
  return g.id
} 


/*  Сбрасывает значение внутреннего счётчика генераттора.
*/
func (g *IDGenerator) ResetID() {

  g.mutex.Lock()
  defer g.mutex.Unlock()

  g.id = 0
}


