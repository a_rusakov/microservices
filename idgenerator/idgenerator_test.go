
package idgenerator

import "testing"


/* Последовательные вызовы метода GetNextID() после создания объекта генератора
   должны возвращать значения начинающиеся с 1 и последовательно увеличивающиеся на 1.
*/
func Test_GetNextID(t *testing.T) {
    
    g := NewIDGenerator()
    var value int64 = g.GetNextID()
    if value != 1 {
        t.Error("Expected 1, got ", value)
    }
    
    value = g.GetNextID()
    if value != 2 {
        t.Error("Expected 2, got ", value)
    }
    
    value = g.GetNextID()
    if value != 3 {
        t.Error("Expected 3, got ", value)
    }
}


/* Вызов метода ResetID() должен установить счётчик уникального значения в начальное состояние.
   Последующий вызов метода GetNextID() долен вернуть значение 1.
*/
func Test_ResetID(t *testing.T) {
    
    g := NewIDGenerator()
    g.GetNextID()

    var value int64 = g.GetNextID()
    if value != 2 {
        t.Error("Expected 2, got ", value)
    }
    
    g.ResetID()
    
    value = g.GetNextID()
    if value != 1 {
        t.Error("Expected 1, got ", value)
    }
}


